# Usage

## Default
The containers `WORKDIR` is `/usr/src/app`. As default it is expected that you mount a local directory into the containers `/usr/src/app/source` directory. In this case the default command executed by the container is: `mvn -f source/pom.xml -s source/mvn.settings.xml clean install`

Considering a project root pom.xml in the current directory, you can use this docker image as an executable like this:

```
$ docker run -v ${PWD}:/usr/src/app/source tomkramer/maven:3.6.0
```

Consider the following as default MAVEN_OPTS:

```
MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1 -Dmaven.repo.local=source/.caches/mvn"
```

So the local cache will be available (and persisted) in the mounted folder.

## Custom
You can easily define your own commands by overrinding the default `CMD` section:

```
$ docker run -v ${PWD}:/usr/src/app/source tomkramer/maven:3.6.0 -f source/pom.xml -Dmaven.test.skip=true package
```

## Bitbucket pipelines
The checkout directory in bitbucket pipelines is avaiable by referencing the $BITBUCKET_CLONE_DIR variable. Since the maven cache is written to the mounted directory, it is possible to define a custom pipeline cache and speed up the succeeding builds. But in this case - for some reason - you need to take care of the cache dirs file permissions with `chmod`.

```
...
pipelines:
  default:
    - step:
        name: build
        trigger: automatic
        caches:
          - customvmn
        script:
          - chmod -R 777 $BITBUCKET_CLONE_DIR/.caches/mvn          
          - docker run -v $BITBUCKET_CLONE_DIR:/usr/src/app/source tomkramer/maven:3.6.0
        services:
          - docker
definitions:
  caches:
    customvmn: $BITBUCKET_CLONE_DIR/.caches/mvn
  services:
    docker:
      memory: 2048
```
